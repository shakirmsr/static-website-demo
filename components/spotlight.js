export default function Spotlight() {
    return (
        <section className="spotlight">
				<div className="container">
					<div className="spotlight__content">
						<h1>An integrated patient payment solution for your practice</h1>
						<p className="p-type1">Elevate your patient payment experience while your practice generates more revenue.</p>
					</div>
					<div className="spotlight__image">
						<img src="/images/spotlight-illustration.png" className="lazyload w-100" alt="spotlight" /> 
					</div>
					<div className="text-center mt-1">
						<a href="#" className="link link--1 link--md" target="_blank">Book Free Demo</a>
					</div>
				</div>
			</section>
    );
}