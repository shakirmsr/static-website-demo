export default function Numbers(){
    return(
        <section className="numbers py-type1 bg-4">
					<div className="container">
						<div className="block-header text-center">
							<h2 className="color-6">By The Numbers</h2>
						</div>
						<div className="numbers__detail">
							<div className="numbers__item">
								<div className="numbers__image">
									<img src="/images/investment-icon.svg" alt="investment raised"/>
								</div>
								<div className="numbers__desc">
									<h5>$60 Million</h5>
									<span>Investment Raised</span>
								</div>
							</div>
							<div className="numbers__item">
								<div className="numbers__image">
									<img src="/images/business-icon.svg" alt="time in business"/>
								</div>
								<div className="numbers__desc">
									<h5>6 Years</h5>
									<span>Length of Time in Business</span>
								</div>
							</div>
							<div className="numbers__item">
								<div className="numbers__image">
									<img src="/images/employees-icon.svg" alt="employees"/>
								</div>
								<div className="numbers__desc">
									<h5>450 Employees</h5>
									<span>No. of Employees</span>
								</div>
							</div>
							<div className="numbers__item">
								<div className="numbers__image">
									<img src="/images/providers-icon.svg" alt="number of providers"/>
								</div>
								<div className="numbers__desc">
									<h5>2000 Providers</h5>
									<span>No. of Providers</span>
								</div>
							</div>
							<div className="numbers__item">
								<div className="numbers__image">
									<img src="/images/records-icon.svg" alt="patient records"/>
								</div>
								<div className="numbers__desc">
									<h5>5 Million</h5>
									<span>Secure patient records</span>
								</div>
							</div>
						</div>
					</div>
				</section>
    );
}