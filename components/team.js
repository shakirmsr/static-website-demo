export default function Team(){
    return (
        <section className="team py-type1">
					<div className="container">
						<div className="team__wrapper">
							<div className="team__block">
								<h3>Team</h3>
								<div className="team__list">
									<div className="team__item">
										<span className="team__head">Abhi Krishna</span>
										<span className="team__label">CEO &amp; Founder</span>
									</div>
									<div className="team__item">
										<span className="team__head">Mark D Huzyak</span>
										<span className="team__label">Co-Founder</span>
									</div>
									<div className="team__item">
										<span className="team__head">Kevin Cook</span>
										<span className="team__label">VP, Customer Success</span>
									</div>
									<div className="team__item">
										<span className="team__head">Jim Gerson</span>
										<span className="team__label">VP, Sales &amp; Marketing</span>
									</div>
									<div className="team__item">
										<span className="team__head">Arjun Satheesh</span>
										<span className="team__label">Director, RCM</span>
									</div>
									<div className="team__item">
										<span className="team__head">Dana Horsch</span>
										<span className="team__label">Manager, Customer Support</span>
									</div>
									<div className="team__item">
										<span className="team__head">Varun Nelson</span>
										<span className="team__label">VP, Product Delivery</span>
									</div>
								</div>
							</div>
							<div className="team__block">
								<h3>Investors</h3>
								<div className="team__list">
									<div className="team__item">
										<span className="team__head">EIGHT ROADS</span>
										<a href="https://eightroads.com/en/" className="team__label" target="_blank" rel="noreferrer" >www.eightroads.com</a>
									</div>
									<div className="team__item">
										<span className="team__head">STEADVIEW CAPITAL INVESTMENT
											</span>
										<a href="https://www.steadview.com/" className="team__label" target="_blank" rel="noreferrer">www.steadview.com</a>
									</div>
									<div className="team__item">
										<span className="team__head">ACCEL</span>
										<a href="https://www.accel.com/india-home" className="team__label" target="_blank" rel="noreferrer">www.accel.com</a>
									</div>
									<div className="team__item">
										<span className="team__head">F-PRIME</span>
										<a href="https://fprimecapital.com/" className="team__label" target="_blank" rel="noreferrer">www.fprimecapital.com</a>
									</div>
								</div>
							</div>
							<div className="team__block">
								<h3>Partners</h3>
								<div className="team__list">
									<div className="team__item">
										<span className="team__head">DARBY DENTAL</span>
										<a href="https://www.darbydental.com/" className="team__label" target="_blank" rel="noreferrer">www.darbydental.com</a>
									</div>
									<div className="team__item">
										<span className="team__head">CHANGE HEALTHCARE</span>
										<a href="https://www.changehealthcare.com/" className="team__label" target="_blank" rel="noreferrer">www.changehealthcare.com</a>
									</div>
									<div className="team__item">
										<span className="team__head">DENTALXCHANGE</span>
										<a href="https://www.dentalxchange.com/home/Home" className="team__label" target="_blank" rel="noreferrer">www.dentalxchange.com</a>
									</div>
									<div className="team__item">
										<span className="team__head">DOSESPOT</span>
										<a href="https://www.dosespot.com/" className="team__label" target="_blank" rel="noreferrer">www.dosespot.com</a>
									</div>
									<div className="team__item">
										<span className="team__head">CARDCONNECT</span>
										<a href="https://cardconnect.com/" className="team__label" target="_blank" rel="noreferrer">www.cardconnect.com</a>
									</div>
									<div className="team__item">
										<span className="team__head">DEPLOY DENTAL</span>
										<a href="https://www.deploydental.com/" className="team__label" target="_blank" rel="noreferrer">www.deploydental.com</a>
									</div>
									<div className="team__item">
										<span className="team__head">DARKHORSE TECH</span>
										<a href="https://darkhorsetech.com/" className="team__label" target="_blank" rel="noreferrer">www.darkhorsetech.com</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
    );
}