export default function Benefits(){
    return (
		<div>
		<span className="anchor-spacer" id="benefits"></span>
        <section className="benefits py-type1">
					<div className="container">
						<div className="block-header text-center">
							<h2>Key Benefits</h2>
							<p className="p-type1">100% safe and secure platform to operate your business efficiently.</p>
						</div>
						<div className="benefits__items">
							<div className="benefits__item">
								<div className="benefits__image">
									<img src="/images/patient-experience.svg" alt="patient experience"/>
								</div>
								<div className="benefits__desc">
									<h3>Enhanced patient experience</h3>
									<p>With &ldquo;card on file&rdquo;, patients can effortlessly check out of the practice post treatment with your practice collecting payments instantly. Your practice gets paid, and your patients get a flawless payment experience. </p>
									<p>With multiple payment options, patients now have the flexibility to pay anywhere, anytime.</p>
								</div>
							</div>
							<div className="benefits__item">
								<div className="benefits__image">
									<img src="/images/practice-workflow.svg" alt="practice workflow"/>
								</div>
								<div className="benefits__desc">
									<h3>Optimized practice workflow</h3>
									<p>With CareStack PMS integration, your billing, and payment process will be simplified, saving you time and money on payment posting, patient follow-ups, and collection calls. With increased billing efficiency, the office will be able to accommodate more patients, resulting in increased revenue.
									</p>
								</div>
							</div>
							<div className="benefits__item">
								<div className="benefits__image">
									<img src="/images/patient-experience.svg" alt="patient ar collections"/>
								</div>
								<div className="benefits__desc">
									<h3>Improve Patient AR Collections</h3>
									<p>CS Pay can help you curtail lengthy follow-ups like those resulting from a patient&rsquo;s insurance carrier denial or due to underpaid claims. With &ldquo;card on file&rdquo;, collect your recurring due payments faster. Now, maximize your collections effortlessly with relatively no downtime.
									</p>
								</div>
							</div>
							<div className="benefits__item">
								<div className="benefits__image">
									<img src="/images/practice-workflow.svg" alt="support team"/>
								</div>
								<div className="benefits__desc">
									<h3>Dedicated and trusted support team </h3>
									<p>When you are with CS Pay, our support team will be there for you, every step of the way. Enjoy unparalleled support with a dedicated Account management and Support team. Reach out to us via email, phone, and chat.

									</p>
								</div>
							</div>
						</div>
						<div className="text-center mt-1">
							<a href="#" className="link link--1 link--lg" target="_blank">Book Free Demo</a>
						</div>
					</div>
				</section>
		</div>
    );
}