import '../loader'
export default function FaqBlock(){
    return(
        <section className="faq py-type1">
					<div className="container">
						<div className="block-header text-center">
							<h1>Frequently Asked Questions</h1>
						</div>
						<div className="faq__listing">
							<div className="faq__item">
								<div className="faq__head open">
									<h6>When will the funds from the credit card transactions arrive in my bank account?</h6>
								</div>
								<div className="faq__desc">
									<p>Payments made on any business day before 8 p.m. EST are deposited to the merchant&rsquo;s bank account within the next business day through our exclusive &lsquo;Next Day Funding&rsquo; feature. </p>
								</div>
							</div>
							<div className="faq__item">
								<div className="faq__head">
									<h6>What will it cost to process payments?</h6>
								</div>
								<div className="faq__desc">
									<p>Payment processing costs are 2.9% for all card present and card not present transactions, 3.4% for all Amex payments, and 0.9% for ACH transactions, plus 30c per transaction. We also offer card on file, tokenization, auto card updater, and next-day funding at no additional cost</p>
								</div>
							</div>
							<div className="faq__item">
								<div className="faq__head">
									<h6>What happens if a patient does not want to save the card on file?</h6>
								</div>
								<div className="faq__desc">
									<p>There is no need to be concerned. By inputting the card details, the payment can be made without the card being saved on file. Saving a card on file, on the other hand, cuts down on billing time and boosts practice collection in the event that a patient underpays.</p>
								</div>
							</div>
							<div className="faq__item">
								<div className="faq__head">
									<h6>When you first start, how long does it take you to get going?</h6>
								</div>
								<div className="faq__desc">
									<p>Spare us 15 minutes, we can get you online right away. We can get you up and running on the call if you schedule a demo with us. You&rsquo;ll need your practice&rsquo;s direct deposit information accessible so we can set up a merchant account for you and start receiving payments.</p>
								</div>
							</div>
							<div className="faq__item">
								<div className="faq__head">
									<h6>What happens when a tokenized card expires?</h6>
								</div>
								<div className="faq__desc">
									<p>With the new state-of-the-art auto-updater technology, CS Pay will automatically update the patient&rsquo;s new card info when their old card expires. The new card will be automatically tokenized.</p>
								</div>
							</div>
							<div className="faq__item">
								<div className="faq__head">
									<h6>What kinds of terminals are compatible with CS Pay? </h6>
								</div>
								<div className="faq__desc">
									<p>CS Pay card transactions work through Dejavoo QD4 touch screen terminals. Cards can be swiped, dipped, and tapped. CS pay also gives you the added benefit of text to pay. Which means that your patients can pay their bills at home.</p>
								</div>
							</div>
							<div className="faq__item">
								<div className="faq__head">
									<h6>Will I be able to partially refund payments received through CS Pay? </h6>
								</div>
								<div className="faq__desc">
									<p>Yes, payments received through CS Pay can be directly, fully or partially refunded from the PMS or the CS Pay portal. Since CS Pay is tightly integrated with Carestack, you would not need to go to another portal to initiate refunds.</p>
								</div>
							</div>
							<div className="faq__item">
								<div className="faq__head">
									<h6>What is CSPay?</h6>
								</div>
								<div className="faq__desc">
									<p>CS Pay is an integrated payment platform offered by Carestack to its clients for processing patient payments. It acts as a payment facilitator. CS Pay provides features like card on file, tokenization, etc. CS Pay also helps the practices improve their collections by providing an integrated payment solution along with the ability to keep patient&rsquo;s card on file.</p>
								</div>
							</div>
						</div>
						
					</div>
				</section>
    );
}