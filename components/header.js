import Link from 'next/link';
import { useRouter } from "next/router";
export default function Header({children}) {
	const router = useRouter()
    return (
        <>
            <header className="global-header">
			<div className="container">
				<span className="logo">
					<Link href="/">
						<a>
							<img src="/images/carestack-logo.svg" alt="carestack cspay" />
						</a>
					</Link>
				</span>
				<div className="header-nav-block">
					<nav className="global-nav" role="navigation">
						<ul>
							
							{/* {router.pathname == "/" ? <li><a href="#why-cspay">Why CS Pay</a></li> :<li><a href="/#why-cspay">Why CS Pay</a></li> }*/}

                            <li className={router.pathname == "/why-cspay" ? "active" : ""}>
								{/* <Link href="/#why-cspay"> */}
								<Link href="/why-cspay">
									<a>Why CS Pay</a>
								</Link>
							</li>
                            <li className={router.pathname == "/benefits" ? "active" : ""}>
                                {/* <Link href="/#benefits"> */}
                                <Link href="/benefits">
                                    <a>Benefits</a>
                                </Link>
                            </li>
                            <li className={router.pathname == "/pricing" ? "active" : ""}>
                                {/* <Link href="/#pricing"> */}
                                <Link href="/pricing">
                                    <a>Pricing</a>
                                </Link>
                            </li>
							
                            <li className={router.pathname == "/faq" ? "active" : ""}>
                                <Link href="/faq">
                                    <a>FAQ&rsquo;s</a>
                                </Link>
                            </li>
                            <li className={router.pathname == "/company" ? "active" : ""}>
                                <Link href="/company">
                                    <a>Company</a>
                                </Link>
                            </li>
                            
						</ul>
					</nav>
					<div className="login">
						<span className="login-btn">Login</span>
						<div className="login__dropdown">
							<div className="login__link">
								<a href="#">
									<img src="/images/login-icon.svg" alt="login" />
									<div className="login__item">
										<span>Existing Customer</span>
										<p>Login to CSPay portal</p>
									</div>
								</a>
							</div>
							<div className="login__link">
								<a href="#">
									<img src="/images/login-icon.svg" alt="login" />
									<div className="login__item">
										<span>New Customer</span>
										<p>Book a demo</p>
									</div>
								</a>
							</div>
						</div>
					</div>
					<a href="#" className="link link--1 link--md">Book Free Demo</a>
				</div>
				<button id="menuToggle"></button>
			</div>
		</header>
		
		<nav className="global-nav nav-for-mobile" role="navigation">
			<ul>
				<li className={router.pathname == "/why-cspay" ? "active" : ""}>
					{/* <Link href="/#why-cspay"> */}
					<Link href="/why-cspay">
						<a>Why CS Pay</a>
					</Link>
				</li>
				<li className={router.pathname == "/benefits" ? "active" : ""}>
					{/* <Link href="/#benefits"> */}
					<Link href="/benefits">
						<a>Benefits</a>
					</Link>
				</li>
				<li className={router.pathname == "/pricing" ? "active" : ""}>
					{/* <Link href="/#pricing"> */}
					<Link href="/pricing">
						<a>Pricing</a>
					</Link>
				</li>
				
				<li className={router.pathname == "/faq" ? "active" : ""}>
					<Link href="/faq">
						<a>FAQ&rsquo;s</a>
					</Link>
				</li>
				<li className={router.pathname == "/company" ? "active" : ""}>
					<Link href="/company">
						<a>Company</a>
					</Link>
				</li>
			</ul>
			<div className="login-mask"></div>
			<div className="login">
				<span className="login-btn">Login</span>
				<div className="login__dropdown">
					<div className="login__link">
						<a href="#">
							<img src="/images/login-icon.svg" alt="login" />
							<div className="login__item">
								<span>Existing Customer</span>
								<p>Login to CSPay portal</p>
							</div>
						</a>
					</div>
					<div className="login__link">
						<a href="#">
							<img src="/images/login-icon.svg" alt="login" />
							<div className="login__item">
								<span>New Customer</span>
								<p>Book a demo</p>
							</div>
						</a>
					</div>
				</div>
			</div>
		</nav>
        </>
    )
}