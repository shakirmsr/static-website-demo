
import Benefits from "../components/benefitsBlock"
import BannerBlue from "../components/bannerBlue"
import Head from "next/head"

export default function BenefitsDesc() {
  return (
    <>
        <Head>
            <title>CSPay | Benefits</title>
        </Head>
      <div className="content-area">
        <Benefits></Benefits>
        <BannerBlue></BannerBlue>
      </div>
    </>
  )
}
