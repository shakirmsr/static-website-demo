import FaqBlock from "../components/faqBlock"
import BannerBlue from "../components/bannerBlue"
import Head from "next/head"
export default function Faq() {
    return (
      <>
        <Head>
          <title>CSPay | Faq</title>
        </Head>
        <div className="content-area">
          <FaqBlock></FaqBlock>
          <BannerBlue></BannerBlue>
        </div>
      </>
    )
  }