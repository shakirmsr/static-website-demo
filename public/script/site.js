
(function ($) {
    var mob = window.matchMedia('(max-width: 767px)').matches;
    var tab = window.matchMedia('(max-width: 1023px)').matches;
    var lg = window.matchMedia('(max-width: 1199px)').matches;
    $(window).on("load", function (e) {
      console.log("loaded");
        $("#loader").fadeOut();
    });
    $(function () {
      $.fn.isInViewport = function () {
        var elementTop = $(this).offset().top;
        var elementBottom = elementTop + $(this).outerHeight();
        var viewportTop = $(window).scrollTop();
        var viewportBottom = viewportTop + $(window).height();
        return elementBottom > viewportTop && elementTop < viewportBottom;
      };

      triggerScroll();
      mobileNav();
      login();
      faqAccordion();
    });

    function faqAccordion(){
      // $('.faq__head').on('click',function(){
        $(document).on('click', '.faq__head', function(){
        if(!$(this).hasClass('open')){
          $(this).addClass('open').siblings('.faq__desc').slideDown();
          $(this).parents('.faq__item').siblings('.faq__item').find('.open').removeClass('open').siblings('.faq__desc').slideUp();
        }
      });
    }
    function login(){
      
      $('.login').on('click', function(){
        if($(this).hasClass('show-login')){
          $(this).removeClass('show-login');
          $('.login-mask').removeClass('show-mask');
        }
        else{
          
          $(this).addClass('show-login');
          $('.login-mask').addClass('show-mask');
        }
      });
      $('.login-mask').on('click',function(){
        $('.login').trigger('click');
      })
    }

    function mobileNav(){
      $('#menuToggle').on('click', function(){
        if($(this).hasClass('opened')){
          $(this).removeClass('opened');
          $('.global-nav').removeClass('show');
        }
        else{
          $(this).addClass('opened');
          $('.global-nav').addClass('show');
        }

      })
    }
    function scrollToHash(hash){
      var hashValue;
      let headerHeight = lg? 64 : 72;
      if(hash){
        hashValue = hash;
      }
      else{
        hashValue = window.location.hash;
      }
      if(hashValue){
        console.log(headerHeight);
        $("html, body").animate({ 
          scrollTop: $(hashValue).offset().top - headerHeight
        }, 1000);
      }
      hashValue='';
    }

    function triggerScroll(){
      // $('a[href^="#"]').click(function(){
      //   if($('#menuToggle').hasClass('opened')){
      //     $('#menuToggle').trigger('click')
      //   }
      //   // var id = $(this).attr('href');
      //   // scrollToHash(id);
      // });

      //for mobile
      $('.global-nav li a').on('click', function(){
        if($('#menuToggle').hasClass('opened')){
          $('#menuToggle').trigger('click');
        }
      })
    }
  
    // $(function () {
    //   setTimeout(function () {
    //     AOS.init({
    //       once: true
    //     });
    //   }, 1000);
    // });
  })(jQuery);
  